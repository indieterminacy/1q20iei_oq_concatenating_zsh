#!/usr/bin/zsh

function xq_compile() {

    # iq_variables
    local tool
    local input
    local gqg_red
    local gqg_reset

    # iq_arguments
    tool="$1"
    input="$2"

    # gqg_colors
    gqg_red="$(tput setaf 1)"
    gqg_reset="$(tput sgr0)"

    function iq_missing_tool() {

        # iq_variables
        local gqg_red
        local gqg_reset

        # iq_arguments
        zparseopts -M -D -E -A MyVariableNameHere -- \
            t f -tool=t -focus=f

        # gq_colors
        red="$(tput setaf 1)"
        reset="$(tput sgr0)"

        # iq_contexts
        ## iq_missing_tool
        if (( ${+MyVariableNameHere[-t]} )); then

            ### iq_arguments
            local dummy_tool
            local tool

            ### rq_calls
            echo -e "\tSelect Tool:"
            echo -e "\t$gqg_red    d$gqg_reset Vim"

            read dummy_tool

            ### iq_contexts
            if [[ $dummy_tool =~ ^[dD]$ ]] then
                tool="vim"
            else
                echo -e "$gqg_red\tNo selection made$gqg_reset"
                echo -e ""
                iq_missing_tool --tool
            fi

            ### rq_calls
            local dummy_focus
            echo -e "\tRecompile $gqg_red$tool$gqg_reset area:"
            echo -e "\t$gqg_red    i$gqg_reset Mappings"
            echo -e "\t$gqg_red    j$gqg_reset Settings"
            echo -e "\t$gqg_red    o$gqg_reset Plugins"
            read dummy_focus

            ### iq_contexts
            if [[ $dummy_focus =~ ^[iI]$ ]] then
                input="mappings"
            elif [[ $dummy_focus =~ ^[jJ]$ ]] then
                input="settings"
            elif [[ $dummy_focus =~ ^[oO]$ ]] then
                input="plugins"
            else
                input="  "
            #     echo -e "$gqg_red\tNo selection made$gqg_reset"
            #     echo -e ""
                # read dummy_focus
                # iq_missing_tool --focus
            fi

            ### rq_calls
            xq_compile $tool $input
        fi

        ## iq_missing_focus
        if (( ${+MyVariableNameHere[-f]} )); then

            ### iq_arguments
            local dummy_focus
            local tool
            # tool="$1"

            ### rq_calls
            # echo -e "\tRecompile $gqg_red$tool$gqg_reset focus area:"
            echo -e "\tRecompile focus area:"
            echo -e "\t$gqg_red    i$gqg_reset Mappings"
            echo -e "\t$gqg_red    j$gqg_reset Settings"
            echo -e "\t$gqg_red    o$gqg_reset Plugins"
            read dummy_focus

            ### iq_contexts
            if [[ $dummy_focus =~ ^[iI]$ ]] then
                input="mappings"
            elif [[ $dummy_focus =~ ^[jJ]$ ]] then
                input="settings"
            elif [[ $dummy_focus =~ ^[oO]$ ]] then
                input="plugins"
            else
                echo "$gqg_red\tNo focus selection made$gqg_reset"
                echo -e ""
                iq_missing_tool -f
            fi

            ### rq_calls
            xq_compile $tool $input
        fi
    }

    # nq_pathways
    ## iq_contexts
    ### iq_arguments_missing
    if [ ! $@ ]; then
        echo -e "$gqg_red    Missing arguments$gqg_reset"
        echo -e ""
        iq_missing_tool "--tool"
        # xq_compile $tool
        # $input=iq_missing_tool --focus
    fi

    if [ ! $input ]; then
        echo -e "$gqg_red    Missing focus argument$gqg_reset"
        echo -e ""
        iq_missing_tool --focus
    fi

    ### oqo_plugins_pathways
    function nq_oq_pathways_tool() {

        # tq_arrays
        typeset -A nq_sources_pathway
        typeset -A tq_sources_file
        typeset -A nq_compiled_pathway
        typeset -A tq_compiled_file

        # iq_arguments
        zparseopts -M -D -E -A MyVariableNameHere -- \
            -file_export -file_import -pathway_export

        # iq_contexts
        if [[ $tool =~ "vim" ]]; then
            # TODO Update pathways
            nq_sources_pathway[qiuy]=~/60o_Flow_Qiuy/50_Environment
            nq_sources_pathway[folder]=nq_pathways/oq_vim/oqo_qiuy
            tq_sources_file[mappings]=iq_pathways_files_mappings_vim
            tq_sources_file[plugins]=oqo_pathways_files_plugins_vim
            tq_sources_file[settings]=jq_pathways_files_settings_vim
            nq_compiled_pathway[qiuy]=~/60o_Doc_Qiuy/10_Activity
            nq_compiled_pathway[folder]=dq_text_editors/oq_vim/oqo_qiuy/jq_configuration
            tq_compiled_file[mappings]=iq_mappings_compiled_vim
            tq_compiled_file[plugins]=oqo_plugins_compiled_vim
            tq_compiled_file[settings]=jq_settings_compiled_vim
        fi

        # rq_calls
        ## iq_contexts
        if (( ${+MyVariableNameHere[--file_import]} )); then
            tq_file=(${nq_sources_pathway[qiuy]}'/'${nq_sources_pathway[folder]}'/'${tq_sources_file[$input]})
        fi


        if (( ${+MyVariableNameHere[--file_export]} )); then
            tq_file=(${nq_compiled_pathway[qiuy]}'/'${nq_compiled_pathway[folder]}'/'${tq_compiled_file[$input]})
        fi

        ## rq_calls
        print $tq_file
    }

    function uq_parse_file_line() {

        # oqo_scripts
        zmodload zsh/mapfile

        # iq_arguments
        local tool
        local iq_file_import
        local iq_file_export
        local pattern1
        local pattern2

        tool="$1"

        # rq_calls
        iq_file_import=$(nq_oq_pathways_tool $tool "--file_import")
        iq_file_export=$(nq_oq_pathways_tool $tool "--file_export")

        # uq_parsing
        FLINES=( "${(f)mapfile[$iq_file_import]}" )
        LIST="${mapfile[$iq_file_import]}" # Not required unless stuff uses it
        integer POS=1             # Not required unless stuff uses it
        integer SIZE=$#FLINES     # Number of lines, not required unless stuff uses it

        # uq_parsing
        ## iq_contexts
        if [[ $tool =~ "vim" ]]; then
            pattern1='^\"'
            pattern2='^\#'
        fi

        for ITEM in $FLINES; do
            if [[ $ITEM =~ $pattern1 || $pattern2 ]]; then
                command1="--strip"
                [[ -f $ITEM ]] && uq_content_append $ITEM $iq_file_export $command1
            fi
            (( POS++ ))
        done
    }

    uq_parse_file_line $tool

}

function mappings() {
    set -o nounset                              # Treat unset variables as an error

    # Parses lines not starting with '#' symbols
    # Loops parsed lines into a concatination
    ### Saves document into README
        # TODO Check
        # TODO Update pathways
    egrep -v '^(#|$|\s*$|\s*\t*#)' ~/50_Environment/50n_Pathways/60o_Vim_Qiuy/sq50i-pathways-files-mappings-vim | \
        xargs cat | \
        sed -e 's/^\".*//g' | \
        sed -e 's/^[ \t]*//g' | \
        # Check
        sed -e 's/^\s*$//d' | \
        # Check
        # TODO Update pathways
        sed -e 's/^\".[:space:]*.*$//d' \
        > ~/50_Environment/50j_Configuration/60o_Vim_Qiuy/50i_Mappings/vq50i-concatenated-files-mappings-vim
        # sed -e 's/^[ \t]*//' > aggregate-cleaned
        # sed 's/^[ \t]+//' | cat > withouttabs
        # sed '/^\"/d' > aggregate-without-comments-deeper.md
}

function plugins() {
    set -o nounset                              # Treat unset variables as an error

    # Parses lines not starting with '#' symbols
    # Loops parsed lines into a concatination
    # Saves document into README
    # TODO Update pathways
    egrep -v '^(#|$|\s*$|\s*\t*#)' ~/50_Environment/50n_Pathways/60o_Vim_Qiuy/sq60o-pathways-files-plugins-vim | \
        xargs cat | \
        sed -e 's/^\".*//' | \
        sed -e 's/^[ \t]*//' | \
        sed -e 's/^\s*$//d' | \
        # TODO Update pathways
        sed -e 's/^\".[:space:]*.*$/d' \
        > ~/50_Environment/50j_Configuration/60o_Vim_Qiuy/60o_Plugins/vq60o-concatinated-files-plugins-vim
        # sed -e 's/^[ \t]*//' > aggregate-cleaned
        # sed 's/^[ \t]+//' | cat > withouttabs
        # sed '/^\"/d' > aggregate-without-comments-deeper.md
}

function settings() {
    set -o nounset                              # Treat unset variables as an error

    # Parses lines not starting with '#' symbols
    # Loops parsed lines into a concatination
    # Saves document into README
    # TODO Update pathways
    egrep -v '^(#|$|\s*$|\s*\t*#)' ~/50_Environment/50n_Pathways/60o_Vim_Qiuy/sq50j-pathways-files-settings-vim | \
        xargs cat | \
        sed -e 's/^\".*//' | \
        sed -e 's/^[ \t]*//' | \
        sed -e 's/^\s*$//d' | \
        sed -e 's/^\".[:space:]*.*$//d' \
        > ~/50_Environment/50j_Configuration/60o_Vim_Qiuy/50j_Settings/vq50j-concatenated-files-settings-vim
        # sed -e 's/^[ \t]*//' > aggregate-cleaned
        # sed 's/^[ \t]+//' | cat > withouttabs
}

function uq_content_append() {
    local iq_file_import
    local iq_file_export
    iq_file_import=$(cat "$1")
    iq_file_export="$2"
    zparseopts -M -D -E -A MyVariableNameHere -- s -strip=s
    echo "IMPORT: "$iq_file_import
    echo "EXPORT: "$iq_file_export
    if (( ${+MyVariableNameHere[-s]} )); then
        echo "TO STRIP"
    fi
}
